<?php get_header();?>

<?php get_template_part( 'template-parts/header' );?>

<?php get_template_part( 'template-parts/menu' );?>

<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background:linear-gradient(rgba(119, 252, 121, 0),rgba(192, 190, 190, 0)), url(<?php echo get_template_directory_uri();?>/images/arbres.jpg); background-size:cover; background-repeat: no-repeat;">
  <div id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <?php the_archive_title( '<h6 class="heading">', '</h6>' ); ?>
        <?php the_archive_description( '<p class="archive-description">', '</p>' ); ?>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->


<main>
<div class="sectiontitle">
    <h6 class="heading">Mes Articles</h6>
    <p>Dolor in fermentum ipsum vel mi mattis venenatis vivamus</p>
</div>
<div id="latest" class="group">
		<?php
    while ( have_posts() ) : the_post();?>
    <article id="goliath" class="one_third">
    <a class="imgover" href="<?php echo get_permalink();?>"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""></a>
			
			<div class="excerpt">
             
                  <h6 class="heading"><?php echo get_the_title(); ?></h6>
            
            <footer><a href="<?php echo get_permalink();?>">Read More <i class="fas fa-angle-right"></i></a></footer>
        </div>
	<div class="entry-links"><?php wp_link_pages(); ?></div>
  </article>
  <?php	endwhile;?>
  </div>
</main>

<?php get_template_part( 'template-parts/footer' );?>

<?php get_footer()?>  
