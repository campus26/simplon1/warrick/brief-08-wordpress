/*const ihtml = document.querySelector("#ihtml");
const phtml = document.querySelector("#phtml");

function formation(a,b){
    let icone = a;
    let paragraphe = b;
    let mediaIP = window.innerWidth;
    if(mediaIP <= 950){
        console.log('test');
        icone.addEventListener("mouseenter", (e)=>{
            paragraphe.style.display = "block";
        });
        icone.addEventListener("mouseleave", (e)=>{
            paragraphe.style.display = "none";
        });
    }
}
formation(ihtml, phtml);
//window.addEventListener("resize", formation(ihtml,phtml));
*/

//GESTION MENU
function menufix(){
    let divhead = document.querySelector("div.header");
    let elem = document.querySelector("header>nav>:nth-child(2)")
    let menu = document.querySelector("#navmenu");
    if (window.scrollX < 1100 && window.scrollY > 45) {
        menu.style.position = "fixed";
        menu.style.top = "0px";
        elem.style.padding = "1%";
        elem.style.backgroundColor = "rgba(255, 255, 255, 0.93)";
        divhead.style.paddingTop = "7.5%";
    } else {
        menu.style.position = "";
        menu.style.top = "";
        elem.style.padding = "";
        elem.style.backgroundColor = "";
        divhead.style.paddingTop = "";
    }
}

window.addEventListener("scroll", menufix);

//LIEN VERS BRIEF
const brief1 = document.querySelector("#briefa");
const brief2 = document.querySelector("#briefb");
const brief3 = document.querySelector("#briefc");
const brief4 = document.querySelector("#briefd");
const brief5 = document.querySelector("#briefe");
const brief6 = document.querySelector("#brieff");
const brief7 = document.querySelector("#briefg");
const brief8 = document.querySelector("#briefh");
const brief9 = document.querySelector("#briefi");

const lien1 = "https://simplonline.co/briefs/3f0e6d15-d28f-40f3-931d-3b1b185f81d8";
const lien2 = "https://simplonline.co/briefs/f66f34d1-441d-437e-b709-79bcca553706";
const lien3 = "https://simplonline.co/briefs/6fabe183-94e8-46fd-a290-cd036a6e1b9e";
const lien4 = "https://simplonline.co/briefs/bdeb23de-6141-45ba-aa4d-be85462ed052";
const lien5 = "https://simplonline.co/briefs/fe419edc-7c4a-481d-a419-b9e21f77dd1a";
const lien6 = "https://simplonline.co/briefs/a799b84a-e5de-473e-87c0-788c031673b6";
const lien7 = "https://simplonline.co/briefs/cd630dcc-4f28-4899-947d-427170ed2ac0";
const lien8 = "https://www.campus26.com/";
const lien9 = "https://www.campus26.com/";

function lienRedirection(a,b){
    a.addEventListener("click", () => {
        document.location.href = b;
    });
}

lienRedirection(brief1,lien1);
lienRedirection(brief2,lien2);
lienRedirection(brief3,lien3);
lienRedirection(brief4,lien4);
lienRedirection(brief5,lien5);
lienRedirection(brief6,lien6);
lienRedirection(brief7,lien7);
lienRedirection(brief8,lien8);
lienRedirection(brief9,lien9);