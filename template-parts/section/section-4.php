<div class="wrapper row2">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <article class="one_third first">
      <h5 class="heading">Portefolio</h5>
      <p>Sed placerat leo nec tincidunt aliquet ipsum mi ultrices magna eu tempor quam dolor eu sem nullam risus turpis rhoncus vel varius consequat laoreet ac neque pellentesque imperdiet sagittis velit vivamus urna ut dictum erat eu massa aliquam nunc urna ornare.</p>
      <p class="btmspace-30">Tincidunt quis posuere id justo aenean velit ipsum tincidunt aliquet lacinia nec pharetra in purus proin auctor justo ac sem.</p>
      <p><a class="btn" href="#">INACTIF<i class="fas fa-angle-right"></i></a></p>
    </article>
    <figure class="one_third"><a class="imgover" href="https://wazzricks.000webhostapp.com/curriculum-vitae"><img src="<?php echo get_template_directory_uri();?>/images/sakura.jpg" alt=""></a></figure>
    <figure class="one_third"><a class="imgover" href="#"><img src="<?php echo get_template_directory_uri();?>/images/sakura.jpg" alt=""></a></figure>
    <!-- ################################################################################################ -->
  </section>
</div>