<header>
        <aside>
            <div>
                <img src="<?php echo get_template_directory_uri();?>/images/facebook-app-symbol (1).png">
                <img src="<?php echo get_template_directory_uri();?>/images/linked-in-logo-of-two-letters.png">
                <img src="<?php echo get_template_directory_uri();?>/images/g.png">
            </div>
            <div>
                <img src="<?php echo get_template_directory_uri();?>/images/phone-call.png">
                <p>0668541771</p>
                <img src="<?php echo get_template_directory_uri();?>/images/envelope.png">
                <p>warrick.niard43@gmail.com</p>
            </div>
        </aside>
        <nav id="navmenu">
            <div>
                <a href="https://www.campus26.com/">
                    <img src="<?php echo get_template_directory_uri();?>/images/campus.jpg">
                </a>
                <a href="https://simplon.co/">
                    <img src="<?php echo get_template_directory_uri();?>/images/simplon.png">
                </a>
            </div>
            <div id="menu">
                <div><a href="https://warrick-niard-wordpress.000webhostapp.com/">
                        <img src="<?php echo get_template_directory_uri();?>/images/home.png">
                    </a>
                </div>
                <div><a href="#formation">Formations</a></div>
                <div><a href="#competences">Competences</a></div>
                <div><a href="#projets">Projets</a></div>
                <div><a href="#propos">Propos</a></div>
                <div><a href="<?php echo get_template_directory_uri();?>/warrick niard.pdf" target="_blank">
                    <img src="<?php echo get_template_directory_uri();?>/images/cv.jpg">
                    </a>
                </div>
            </div>
        </nav>

        <div class="header">
            <div>
                <h1>La véritable <span class="art">intelligence</span> n'est pas la <span
                        class="art">connaissance</span>
                    mais <span class="art">l'imagination</span></h1>
            </div>
            <div>
                <img src="<?php echo get_template_directory_uri();?>/images/DSC09150-test.png" alt="">
            </div>
        </div>
    </header>