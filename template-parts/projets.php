<section class="projets" id="projets">
            <h2>MES PROJETS</h2>
            <div>
                <div>
                    <article id="briefa" data-aos="flip-left" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 1</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                    <article id="briefb" data-aos="flip-down" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 2</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                    <article id="briefc" data-aos="flip-right" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 3</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                </div>
                <div>
                    <article id="briefd" data-aos="flip-left" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 4</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                    <article id="briefe" data-aos="flip-down" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 5</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                    <article id="brieff" data-aos="flip-right" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 6</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                </div>
                <div>
                    <article id="briefg" data-aos="flip-left" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 7</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                    <article id="briefh" data-aos="flip-down" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 00</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                    <article id="briefi" data-aos="flip-right" data-aos-duration="1000">
                        <img src="<?php echo get_template_directory_uri();?>/images/fond_responsive.png">
                        <div>
                            <h4>BRIEF 00</h4>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </article>
                </div>
                <?php //get_template_part('template-parts/menu');?>
                <?php get_template_part('template-parts/mes-derniers-articles');?>

            </div>
        </section>