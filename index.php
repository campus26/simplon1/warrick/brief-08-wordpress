<?php get_header();?>
<?php get_template_part( 'template-parts/header' );?>
<?php get_template_part( 'template-parts/formation' );?>
<?php get_template_part( 'template-parts/competences' );?>
<?php get_template_part( 'template-parts/projets' );?>
<?php get_template_part( 'template-parts/propos' );?>
<?php get_template_part( 'template-parts/footer' );?>
<?php get_footer();?>